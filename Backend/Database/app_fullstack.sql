-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Sep 2022 pada 22.52
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_fullstack`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `much_room` int(11) NOT NULL,
  `number_of_bathrooms` int(11) NOT NULL,
  `meter` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `price` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `type`, `owner`, `much_room`, `number_of_bathrooms`, `meter`, `name`, `image`, `url`, `price`, `address`, `city`, `postal_code`, `createdAt`, `updatedAt`) VALUES
(33, 'Rumah', 'Arif Yudi Suryo Utomo', 2, 2, 800, 'image name', '30556818822f2bf9678d33b798a119ba.png', 'http://localhost:5000/Images/30556818822f2bf9678d33b798a119ba.png', 'Rp. 800 juta', 'Semarang ', 'Rembang', 1234, '2022-09-02 09:40:26', '2022-09-02 09:40:26'),
(34, 'Rumah', 'Arif Yudi Suryo ', 3, 3, 900, 'image name', 'a869b05d9f755649ce5e4ce040c4045c.png', 'http://localhost:5000/Images/a869b05d9f755649ce5e4ce040c4045c.png', 'Rp. 900 juta', 'Semarang ', 'Rembang', 1234, '2022-09-02 09:45:39', '2022-09-02 09:45:39');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
