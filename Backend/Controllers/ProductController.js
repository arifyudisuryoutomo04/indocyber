import pM from "../Models/ProductModels.js";
import path from "path";
import fs from "fs";

export const getProducts = async(req, res) => {
    try {
        const gP = await pM.findAll();
        res.json(gP);
    } catch (error) {
        console.log("Get products error", error.message);
    }
};

export const getProductById = async(req, res) => {
    try {
        const gP1 = await pM.findOne({
            where: {
                id: req.params.id
            }
        });
        res.json(gP1);
    } catch (error) {
        console.log("Get products error", error.message);
    }
};

export const saveProduct = (req, res) => {
    if (req.files === null)
        return res.status(400).json({ msg: "No file uploaded" });
    const type = req.body.type;
    const owner = req.body.owner;
    const much_room = req.body.much_room;
    const number_of_bathrooms = req.body.number_of_bathrooms;
    const meter = req.body.meter;
    const name = req.body.title;
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/Images/${fileName}`;
    const price = req.body.price;
    const address = req.body.address;
    const city = req.body.city;
    const postal_code = req.body.postal_code;
    const allowedType = [".png", ".jpg", "jpeg"];
    if (!allowedType.includes(ext.toLowerCase()))
        return res.status(422).json({ msg: "Invalid Images" });
    if (fileSize > 5000000)
        return res.status(422).json({ msg: "Images must be less than 5MB" });

    file.mv(`./Public/Images/${fileName}`, async(err) => {
        if (err) {
            return res.status(500).json({ msg: err.message });
        }
        try {
            await pM.create({
                type: type,
                owner: owner,
                much_room: much_room,
                number_of_bathrooms: number_of_bathrooms,
                meter: meter,
                name: name,
                image: fileName,
                url: url,
                price: price,
                address: address,
                city: city,
                postal_code: postal_code
            });
            res.status(201).json({
                msg: "Products Created Success"
            });
        } catch (error) {
            console.log(error.message);
        }
    });
};

export const deleteProduct = async(req, res) => {
    const dP1 = await pM.findOne({
        where: {
            id: req.params.id
        }
    });
    if (!dP1) return res.status(404).json({ msg: "No Data Found" });

    try {
        const FilePath = `./Public/Images/${dP1.image}`;
        fs.unlinkSync(FilePath);
        await pM.destroy({
            where: {
                id: req.params.id
            }
        });
        res.status(200).json({ msg: "Products Deleted Success" });
    } catch (error) {
        console.log(error.message);
    }
};