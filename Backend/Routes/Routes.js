import express from "express";
import {
    getProducts,
    getProductById,
    saveProduct,
    deleteProduct
} from "../Controllers/ProductController.js";

const r = express.Router();

r.get("/products", getProducts);
r.get("/products/:id", getProductById);
r.post("/products", saveProduct);
r.delete("/products/:id", deleteProduct);

export default r;