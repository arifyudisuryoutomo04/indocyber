import { Sequelize } from "sequelize";
import db from "../Config/Database.js";

const { DataTypes } = Sequelize;

const Product = db.define(
    "products", {
        type: DataTypes.STRING,
        owner: DataTypes.STRING,
        much_room: DataTypes.STRING,
        number_of_bathrooms: DataTypes.STRING,
        meter: DataTypes.STRING,
        name: DataTypes.STRING,
        image: DataTypes.STRING,
        url: DataTypes.STRING,
        price: DataTypes.STRING,
        address: DataTypes.STRING,
        city: DataTypes.STRING,
        postal_code: DataTypes.STRING
    }, {
        freezeTableName: true
    }
);

export default Product;

//! function to create a product table if there is no product table

// (async() => {
//     await db.sync();
// })();