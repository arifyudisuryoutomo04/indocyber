import { Sequelize } from "sequelize";

const db = new Sequelize("app_fullstack", "root", "", {
    host: "localhost",
    dialect: "mysql"
});

export default db;