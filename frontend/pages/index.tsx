import type { NextPage } from "next";
import {
  Container,
  SimpleGrid,
  Image,
  Flex,
  Heading,
  Text,
  Stack,
  StackDivider,
  Icon,
  useColorModeValue,
  Box,
  HStack,
  VStack,
  Divider,
  Grid,
  GridItem,
  chakra,
  Button
} from "@chakra-ui/react";
import {
  IoAnalyticsSharp,
  IoLogoBitcoin,
  IoSearchSharp
} from "react-icons/io5";
import { FcAssistant, FcDonate, FcInTransit } from "react-icons/fc";
import { CheckIcon } from "@chakra-ui/icons";
import Coursel from "../components/coursel";
import { ReactElement } from "react";

interface FeatureProps {
  text: string;
  iconBg: string;
  icon?: ReactElement;
}
interface FeaturesProps {
  title: string;
  text: string;
  icon: ReactElement;
}
interface LastFeatureProps {
  heading: string;
  text: string;
}
const Home: NextPage = () => {
  return (
    <>
      <Coursel />

      <Container maxW={"5xl"} py={12}>
        <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10}>
          <Stack spacing={4}>
            <Text
              textTransform={"uppercase"}
              color={"blue.400"}
              fontWeight={600}
              fontSize={"sm"}
              bg={useColorModeValue("blue.50", "blue.900")}
              p={2}
              alignSelf={"flex-start"}
              rounded={"md"}
            >
              Our Story
            </Text>
            <Heading>A digital Product design agency</Heading>
            <Text color={"gray.500"} fontSize={"lg"}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore
            </Text>
            <Stack
              spacing={4}
              divider={
                <StackDivider
                  borderColor={useColorModeValue("gray.100", "gray.700")}
                />
              }
            >
              <Feature
                icon={
                  <Icon
                    as={IoAnalyticsSharp}
                    color={"yellow.500"}
                    w={5}
                    h={5}
                  />
                }
                iconBg={useColorModeValue("yellow.100", "yellow.900")}
                text={"Business Planning"}
              />
              <Feature
                icon={
                  <Icon as={IoLogoBitcoin} color={"green.500"} w={5} h={5} />
                }
                iconBg={useColorModeValue("green.100", "green.900")}
                text={"Financial Planning"}
              />
              <Feature
                icon={
                  <Icon as={IoSearchSharp} color={"purple.500"} w={5} h={5} />
                }
                iconBg={useColorModeValue("purple.100", "purple.900")}
                text={"Market Analysis"}
              />
            </Stack>
          </Stack>
          <Flex>
            <Image
              rounded={"md"}
              alt={"feature image"}
              src={
                "https://images.unsplash.com/photo-1554200876-56c2f25224fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
              }
              objectFit={"cover"}
            />
          </Flex>
        </SimpleGrid>

        <Box paddingTop={"10%"}>
          <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
            <Features
              icon={<Icon as={FcAssistant} w={10} h={10} />}
              title={"Lifetime Support"}
              text={
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore..."
              }
            />
            <Features
              icon={<Icon as={FcDonate} w={10} h={10} />}
              title={"Unlimited Donations"}
              text={
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore..."
              }
            />
            <Features
              icon={<Icon as={FcInTransit} w={10} h={10} />}
              title={"Instant Delivery"}
              text={
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore..."
              }
            />
          </SimpleGrid>
        </Box>

        <Box p={4} paddingTop={"10%"}>
          <Stack spacing={4} as={Container} maxW={"3xl"} textAlign={"center"}>
            <Heading fontSize={"3xl"}>This is the headline</Heading>
            <Text color={"gray.600"} fontSize={"xl"}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua.
            </Text>
          </Stack>

          <Container maxW={"6xl"} mt={10}>
            <SimpleGrid columns={{ base: 1, md: 2, lg: 4 }} spacing={10}>
              {Featur.map((e) => (
                <HStack key={e.id} align={"top"}>
                  <Box color={"green.400"} px={2}>
                    <Icon as={CheckIcon} />
                  </Box>
                  <VStack align={"start"}>
                    <Text fontWeight={600}>{e.title}</Text>
                    <Text color={"gray.600"}>{e.text}</Text>
                  </VStack>
                </HStack>
              ))}
            </SimpleGrid>
          </Container>
        </Box>

        <Box as={Container} maxW="7xl" mt={14} p={4}>
          <Grid
            templateColumns={{
              base: "repeat(1, 1fr)",
              sm: "repeat(2, 1fr)",
              md: "repeat(2, 1fr)"
            }}
            gap={4}
          >
            <GridItem colSpan={1}>
              <VStack alignItems="flex-start" spacing="20px">
                <chakra.h2 fontSize="3xl" fontWeight="700">
                  Medium length title
                </chakra.h2>
                <Button colorScheme="green" size="md">
                  Call To Action
                </Button>
              </VStack>
            </GridItem>
            <GridItem>
              <Flex>
                <chakra.p>
                  Provide your customers a story they would enjoy keeping in
                  mind the objectives of your website. Pay special attention to
                  the tone of voice.
                </chakra.p>
              </Flex>
            </GridItem>
          </Grid>
          <Divider mt={12} mb={12} />
          <Grid
            templateColumns={{
              base: "repeat(1, 1fr)",
              sm: "repeat(2, 1fr)",
              md: "repeat(4, 1fr)"
            }}
            gap={{ base: "8", sm: "12", md: "16" }}
          >
            <LastFeature
              heading={"First Feature"}
              text={"Short text describing one of you features/service"}
            />
            <LastFeature
              heading={"Second Feature"}
              text={"Short text describing one of you features/service"}
            />
            <LastFeature
              heading={"Third Feature"}
              text={"Short text describing one of you features/service"}
            />
            <LastFeature
              heading={"Fourth Feature"}
              text={"Short text describing one of you features/service"}
            />
          </Grid>
        </Box>
      </Container>
    </>
  );
};

const Feature = ({ text, icon, iconBg }: FeatureProps) => {
  return (
    <Stack direction={"row"} align={"center"}>
      <Flex
        w={8}
        h={8}
        align={"center"}
        justify={"center"}
        rounded={"full"}
        bg={iconBg}
      >
        {icon}
      </Flex>
      <Text fontWeight={600}>{text}</Text>
    </Stack>
  );
};
const Features = ({ title, text, icon }: FeaturesProps) => {
  return (
    <Stack>
      <Flex
        w={16}
        h={16}
        align={"center"}
        justify={"center"}
        color={"white"}
        rounded={"full"}
        bg={"gray.100"}
        mb={1}
      >
        {icon}
      </Flex>
      <Text fontWeight={600}>{title}</Text>
      <Text color={"gray.600"}>{text}</Text>
    </Stack>
  );
};
const Featur = Array.apply(null, Array(8)).map(function (x, i) {
  return {
    id: i,
    title: "Lorem ipsum dolor sit amet",
    text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam."
  };
});

const LastFeature = ({ heading, text }: LastFeatureProps) => {
  return (
    <GridItem>
      <chakra.h3 fontSize="xl" fontWeight="600">
        {heading}
      </chakra.h3>
      <chakra.p>{text}</chakra.p>
    </GridItem>
  );
};

export default Home;
