import {
  Box,
  Container,
  Stack,
  Text,
  Image,
  Flex,
  VStack,
  Button,
  Heading,
  SimpleGrid,
  useColorModeValue,
  List,
  ListItem,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel
} from "@chakra-ui/react";
import React, { useState, useEffect } from "react";

export const getStaticPaths = async () => {
  const res = await fetch("http://localhost:5000/products");
  const data = await res.json();

  const paths = data.map((x: { id: { toString: () => any } }) => {
    return {
      params: { id: x.id.toString() }
    };
  });

  return {
    paths,
    fallback: false
  };
};

export const getStaticProps = async (context: { params: { id: any } }) => {
  const id = context.params.id;
  const res = await fetch("http://localhost:5000/products/" + id);
  const data = await res.json();

  return {
    props: { x: data }
  };
};

const DetailsProducts = ({ x }: any) => {
  return (
    <>
      <Container maxW={"7xl"}>
        <SimpleGrid
          columns={{ base: 1, lg: 2 }}
          spacing={{ base: 8, md: 10 }}
          py={{ base: 18, md: 24 }}
        >
          <Flex>
            <Image
              rounded={"md"}
              alt={"product image"}
              src={x.url}
              fit={"cover"}
              align={"center"}
              w={"100%"}
              h={{ base: "100%", sm: "400px", lg: "500px" }}
            />
          </Flex>

          <Stack spacing={{ base: 6, md: 10 }}>
            <Box as={"header"}>
              <Text
                fontSize={{ base: "16px", lg: "18px" }}
                color={useColorModeValue("yellow.500", "yellow.300")}
                fontWeight={"500"}
                textTransform={"uppercase"}
                mb={"4"}
              >
                {x.price}
              </Text>
              <Text
                color={useColorModeValue("gray.900", "gray.400")}
                fontWeight={300}
                fontSize={"md"}
              >
                {x.much_room} Rooms | {x.number_of_bathrooms}
                Bath Room | {x.meter} meters
              </Text>
              <Heading
                lineHeight={1.1}
                fontWeight={600}
                fontSize={{ base: "2xl", sm: "4xl", lg: "5xl" }}
              >
                {x.type}
              </Heading>
              <Text
                color={useColorModeValue("gray.900", "gray.400")}
                fontWeight={300}
                fontSize={"md"}
                mt={2}
              >
                Owner:&nbsp;
                {x.owner}
              </Text>
            </Box>
            <Stack spacing={{ base: 4, sm: 6 }} direction={"column"}>
              <VStack spacing={{ base: 4, sm: 6 }}>
                <Text fontSize={"lg"}>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad
                  aliquid amet at delectus doloribus dolorum expedita hic, ipsum
                  maxime modi nam officiis porro, quae, quisquam quos
                  reprehenderit velit? Natus, totam.
                </Text>
              </VStack>
              <Box>
                <Tabs isFitted variant="soft-rounded" colorScheme="gray">
                  <TabList>
                    <Tab>Details</Tab>
                    <Tab>Map</Tab>
                    <Tab>Load</Tab>
                  </TabList>
                  <TabPanels>
                    <TabPanel>
                      <Text
                        fontSize={{ base: "16px", lg: "18px" }}
                        color={useColorModeValue("yellow.500", "yellow.300")}
                        fontWeight={"500"}
                        textTransform={"uppercase"}
                        mb={"4"}
                      >
                        Product Details
                      </Text>
                      <List spacing={2}>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Between lugs:
                          </Text>{" "}
                          20 mm
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Bracelet:
                          </Text>{" "}
                          leather strap
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Case:
                          </Text>{" "}
                          Steel
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Case diameter:
                          </Text>{" "}
                          42 mm
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Dial color:
                          </Text>{" "}
                          Black
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Crystal:
                          </Text>{" "}
                          Domed, scratch‑resistant sapphire crystal with
                          anti‑reflective treatment inside
                        </ListItem>
                        <ListItem>
                          <Text as={"span"} fontWeight={"bold"}>
                            Water resistance:
                          </Text>{" "}
                          5 bar (50 metres / 167 feet){" "}
                        </ListItem>
                      </List>
                    </TabPanel>
                    <TabPanel>
                      <p>Map</p>
                    </TabPanel>
                    <TabPanel>
                      <p>Load</p>
                    </TabPanel>
                  </TabPanels>
                </Tabs>
              </Box>
            </Stack>
            <Button
              rounded={"full"}
              w={"full"}
              mt={8}
              size={"lg"}
              py={"7"}
              bg={useColorModeValue("gray.900", "gray.50")}
              color={useColorModeValue("white", "gray.900")}
              textTransform={"uppercase"}
              _hover={{
                transform: "translateY(2px)",
                boxShadow: "lg"
              }}
            >
              Add to cart
            </Button>
            <Stack
              direction="row"
              alignItems="center"
              justifyContent={"center"}
            ></Stack>
          </Stack>
        </SimpleGrid>
      </Container>
    </>
  );
};
export default DetailsProducts;
