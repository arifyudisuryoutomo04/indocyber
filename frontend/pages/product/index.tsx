/* eslint-disable react-hooks/rules-of-hooks */
import {
  Box,
  Container,
  Stack,
  Text,
  Image,
  Flex,
  Heading,
  SimpleGrid,
  useColorModeValue,
  Link
} from "@chakra-ui/react";
import React from "react";

export const getStaticProps = async () => {
  const res = await fetch("http://localhost:5000/products");
  const data = await res.json();
  return {
    props: { x: data }
  };
};

const Product = ({ x }: any) => {
  return (
    <>
      <Container maxW={"7xl"}>
        {x.map((x: any) => (
          <Link
            key={x.id}
            href={`/product/` + x.id}
            _hover={{
              textDecoration: "none"
            }}
          >
            <Box shadow={"lg"}>
              <SimpleGrid
                columns={{ base: 1, lg: 2 }}
                spacing={{ base: 8, md: 10 }}
                py={{ base: 18, md: 24 }}
              >
                <Flex>
                  <Image
                    rounded={"md"}
                    alt={"product image"}
                    src={x.url}
                    fit={"cover"}
                    align={"center"}
                    w={"100%"}
                    h={{ base: "100%", sm: "400px", lg: "500px" }}
                  />
                </Flex>
                <Container>
                  <Stack spacing={{ base: 6, md: 10 }}>
                    <Box as={"header"}>
                      <Text
                        color={useColorModeValue("gray.900", "gray.400")}
                        fontWeight={300}
                        fontSize={"md"}
                      >
                        {x.much_room} Rooms | {x.number_of_bathrooms}
                        Bath Room | {x.meter} meters
                      </Text>
                      <Heading
                        lineHeight={1.1}
                        fontWeight={600}
                        fontSize={{ base: "2xl", sm: "4xl", lg: "5xl" }}
                      >
                        {x.type}
                      </Heading>
                      <Text
                        color={useColorModeValue("gray.900", "gray.400")}
                        fontWeight={300}
                        fontSize={"md"}
                        mt={2}
                      >
                        Owner:&nbsp;
                        {x.owner}
                      </Text>
                      <Text
                        color={useColorModeValue("gray.900", "gray.400")}
                        fontWeight={300}
                        fontSize={"md"}
                        mt={2}
                      >
                        Location:&nbsp;
                        {x.address}&nbsp;
                        {x.city}&nbsp;
                        {x.postal_code}&nbsp;
                      </Text>
                      <Text
                        color={useColorModeValue("gray.900", "gray.400")}
                        fontWeight={300}
                        fontSize={"md"}
                        mt={2}
                      >
                        {x.price}
                      </Text>
                    </Box>
                  </Stack>
                </Container>
              </SimpleGrid>
            </Box>
          </Link>
        ))}
      </Container>
    </>
  );
};
export default Product;
