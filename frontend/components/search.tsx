import React from "react";
import {
  Button,
  ChakraProvider,
  theme,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  useDisclosure,
  FormControl,
  FormLabel,
  Input,
  Flex,
  Link,
  Stack,
  useColorModeValue,
  Divider,
  Text,
  Grid,
  GridItem,
  Select
} from "@chakra-ui/react";
import { FcMindMap, FcHome, FcCheckmark } from "react-icons/fc";
import Province from "../data-dummy/location.json";
import Type_of_property from "../data-dummy/type_of_property.json";
import Status from "../data-dummy/status.json";

interface searchItem {
  padding: string;
  paddingLeft: string;
}

function Search({ padding, paddingLeft }: searchItem) {
  const Search = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [overlay, setOverlay] = React.useState(<Search />);

  return (
    <ChakraProvider theme={theme}>
      <Button
        color={useColorModeValue("gray.600", "gray.200")}
        fontSize={"sm"}
        fontWeight={500}
        variant={"link"}
        onClick={() => {
          setOverlay(<Search />);
          onOpen();
        }}
        _hover={{
          textDecoration: "none"
        }}
        padding={padding}
        paddingLeft={paddingLeft}
      >
        Search
      </Button>
      <Modal isCentered isOpen={isOpen} onClose={onClose} size="xl">
        {overlay}
        <ModalContent
          color={useColorModeValue("gray.700", "gray.200")}
          bg={useColorModeValue("gray.50", "gray.900")}
        >
          <Stack minH={"100vh"} direction={{ base: "column", md: "row" }}>
            <ModalCloseButton
              color={useColorModeValue("gray.700", "gray.200")}
            />
            <Flex p={8} flex={1} align={"center"} justify={"center"}>
              <Stack spacing={4} w={"full"} maxW={"md"}>
                <FormControl>
                  <FormLabel>Search Location</FormLabel>
                  <Input
                    placeholder="Search Location"
                    borderRadius="full"
                    focusBorderColor="teal.500"
                    borderColor="teal"
                  />
                </FormControl>
                <Stack
                  style={{
                    marginTop: "28px"
                  }}
                >
                  <Grid templateColumns="repeat(5, 1fr)" gap={4}>
                    <GridItem colSpan={2}>
                      <Divider
                        orientation="horizontal"
                        borderColor={useColorModeValue("gray.700", "gray.200")}
                      />
                    </GridItem>
                    <GridItem>
                      <Text textAlign={"center"} marginTop="-12px">
                        Atau
                      </Text>
                    </GridItem>
                    <GridItem colStart={4} colEnd={6}>
                      <Divider
                        orientation="horizontal"
                        borderColor={useColorModeValue("gray.700", "gray.200")}
                      />
                    </GridItem>
                  </Grid>
                </Stack>
                <FormControl>
                  <FormLabel>
                    Pick Location
                    <FcMindMap
                      style={{
                        float: "right"
                      }}
                    />
                  </FormLabel>
                  <Select
                    borderRadius="full"
                    focusBorderColor="teal.500"
                    borderColor="teal"
                  >
                    {Province &&
                      Province.map((x) => (
                        <option key={x.id} value={x.county}>
                          {x.county}
                        </option>
                      ))}
                  </Select>
                </FormControl>
                <FormControl>
                  <FormLabel>
                    Type Of Property
                    <FcHome
                      style={{
                        float: "right"
                      }}
                    />
                  </FormLabel>
                  <Select
                    borderRadius="full"
                    focusBorderColor="teal.500"
                    borderColor="teal"
                  >
                    {Type_of_property &&
                      Type_of_property.map((x) => (
                        <option key={x.id} value={x.name}>
                          {x.name}
                        </option>
                      ))}
                  </Select>
                </FormControl>
                <FormControl>
                  <FormLabel>
                    Status
                    <FcCheckmark
                      style={{
                        float: "right"
                      }}
                    />
                  </FormLabel>
                  <Select
                    borderRadius="full"
                    focusBorderColor="teal.500"
                    borderColor="teal"
                  >
                    {Status &&
                      Status.map((x) => (
                        <option key={x.id} value={x.status}>
                          {x.status}
                        </option>
                      ))}
                  </Select>
                </FormControl>
                <Stack
                  spacing={6}
                  style={{
                    paddingTop: "7%"
                  }}
                >
                  <Link href="/product">
                    <Button
                      variant={"solid"}
                      bg={"teal"}
                      color={"white"}
                      mr={3}
                      width="100%"
                      rounded={"full"}
                      _hover={{
                        bg: "gray.700"
                      }}
                    >
                      Search
                    </Button>
                  </Link>
                </Stack>
              </Stack>
            </Flex>
          </Stack>
        </ModalContent>
      </Modal>
    </ChakraProvider>
  );
}
export default Search;
