<div align="center">
  <a href="https://vocasia.id/">
    <img src="/images/unnamed.png" alt="vocasia" width="480px">
  </a>
  <p><i>Make you competent</i></p>
</div>

## the application is responsive

NOTED: import database in Database folder in backend

## Getting Started Frontend - React.js

First, install all needed dependencies:

```bash
cd frontend
```

```bash
npm install
```

Then, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

To learn more about this project, take a look at the following resources:

- [JavaScript ES6](https://www.w3schools.com/Js/js_es6.asp) - learn about JavaScript ES6 before continue to React
- [TypeScript](https://www.typescriptlang.org/) - this project uses TypeScript as main language
- [React](https://reactjs.org/) - learn about React and React Features (e.g. React Hooks)
- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Chakra UI](https://chakra-ui.com/) - learn Chakra UI. This project uses Chakra UI as UI Library

## Getting Started Backend - Node.js

First, install all needed dependencies:

```bash
cd Backend
```

```bash
npm install
```

Then, run the development server:

```bash
nodemon index
```

Open [http://localhost:5000](http://localhost:5000) with your browser to see the result.

## More

To learn more about this project, take a look at the following resources:

- [cors](https://www.stackhawk.com/blog/nodejs-cors-guide-what-it-is-and-how-to-enable-it/)
- [express](http://expressjs.com/)
- [express-fileupload](https://www.npmjs.com/)
- [mysql2](http://sidorares.github.io/node-mysql2/)
- [nodemon](https://www.npmjs.com/)
- [sequelize](https://sequelize.org/)
